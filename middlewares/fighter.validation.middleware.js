const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { name, health, power, defense } = req.body;

  if (!name) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  const healthRange = health > 0 && health <= 120;
  if (!healthRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'health' should be in range > 0 & <= 120",
    });
  }

  const powerRange = power > 0 && power <= 100;
  if (!power && !powerRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'power' should be in range > 0 & <= 100",
    });
  }

  const defenseRange = defense > 0 && defense <= 10;
  if (!defense && !defenseRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'defense' should be in range > 0 & <= 10",
    });
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const { name, health, power, defense } = req.body;

  if (!name) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  const healthRange = health > 0 && health <= 120;
  if (!healthRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'health' should be in range > 0 & <= 120",
    });
  }

  const powerRange = power > 0 && power <= 100;
  if (!power && !powerRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'power' should be in range > 0 & <= 100",
    });
  }

  const defenseRange = defense > 0 && defense <= 10;
  if (!defense && !defenseRange) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'defense' should be in range > 0 & <= 10",
    });
  }

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
