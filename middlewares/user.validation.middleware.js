const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { firstName, lastName, email, password, phoneNumber } = req.body;

  if (!firstName) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  if (!lastName) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'last name' should not be empty",
    });
  }

  if (!email && !/\S+@gmail.com/.test(email.trim())) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  if (!phoneNumber && !/^(\+380)?(\d{9}$)/.test(phoneNumber)) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  if (!password || password.length < 3) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Password should be longer than 3 symbols",
    });
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const { firstName, lastName, email, password, phoneNumber } = req.body;

  if (!firstName) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'name' should not be empty",
    });
  }

  if (!lastName) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Field 'last name' should not be empty",
    });
  }

  if (!email && !/\S+@gmail.com/.test(email.trim())) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Use gmail email",
    });
  }

  if (!phoneNumber && !/^(\+380)?(\d{9}$)/.test(phoneNumber)) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Phone number must be valid",
    });
  }

  if (password.length < 3) {
    return res.status(400).json({
      status: "error",
      code: 400,
      data: "Bad request",
      message: "Password should be longer than 3 symbols",
    });
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
