const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  const { err, data } = res;
  if (err) {
    const errorResponse = {
      error: true,
      message: res.err.message,
    };
    res.status(400).json(errorResponse);
    next(err);
  } else {
    res.status(200).json(data);
    next();
  }
  //   next();
};

exports.responseMiddleware = responseMiddleware;
