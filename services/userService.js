const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  createNew(user) {
    return UserRepository.create(user);
  }

  getAll() {
    return UserRepository.getAll();
  }

  update(id, updatedUser) {
    return UserRepository.update(id, updatedUser);
  }

  delete(id) {
    return UserRepository.delete(id);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
