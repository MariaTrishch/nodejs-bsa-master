const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  createNew(fighter) {
    return FighterRepository.create(fighter);
  }

  getAll() {
    return FighterRepository.getAll();
  }

  update(id, updatedFighter) {
    return FighterRepository.update(id, updatedFighter);
  }

  delete(id) {
    return FighterRepository.delete(id);
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getFighter(id) {
    return this.search((item) => item.id === id);
  }
}

module.exports = new FighterService();
