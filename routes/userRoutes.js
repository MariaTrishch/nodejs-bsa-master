const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    try {
      const { email, phoneNumber } = req.body;
      const userEmail = UserService.search({ email });
      const userPhone = UserService.search({ phoneNumber });
      if (userEmail || userPhone) {
        return res.status(409).json({
          status: "error",
          code: 409,
          data: "Conflict",
          message: "Email is already use",
        });
      }
      const newUser = UserService.createNew({
        ...req.body,
      });
      return res.status(201).json({
        status: "success",
        code: 201,
        data: {
          id: newUser.id,
          email: newUser.email,
          firstName: newUser.firstName,
          lastName: newUser.lastName,
          phoneNumber: newUser.phoneNumber,
        },
      });
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    try {
      const { id } = req.params;
      const dataToUpdate = req.body;
      const updatedUser = UserService.update(id, dataToUpdate);
      return res.json({
        status: "Success",
        code: 200,
        message: "Users information has just updated",
        data: {
          updatedUser,
        },
      });
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/",
  (req, res, next) => {
    try {
      const allUsers = UserService.getAll();

      return res.json({
        status: "Success",
        code: 200,
        message: "All users",
        data: {
          ...allUsers,
        },
      });
    } catch (err) {
      res.status(404);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      const { id } = req.params;
      const user = UserService.search(id);

      return res.status(200).json({
        status: "success",
        code: 200,
        data: {
          id: user.id,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          phoneNumber: user.phoneNumber,
        },
      });
    } catch (err) {
      res.status(404);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const { id } = req.params;
      const deletedUser = UserService.delete(id);
      if (deletedUser) {
        return res.json({
          status: "Success",
          code: 200,
          message: "User deleted",
          data: {
            ...deletedUser,
          },
        });
      } else {
        return res.status(404).json({
          error: true,
          status: "Error",
          code: 404,
          message: "Not Found",
        });
      }
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
