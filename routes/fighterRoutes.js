const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter
router.get(
  "/",
  (req, res, next) => {
    try {
      const allFighters = FighterService.getAll();

      return res.json({
        status: "Success",
        code: 200,
        message: "All fighters",
        data: {
          ...allFighters,
        },
      });
    } catch (err) {
      res.status(404);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      const { id } = req.params;
      const fighter = FighterService.getFighter({ id });

      return res.status(200).json({
        status: "success",
        code: 200,
        data: {
          id: fighter.id,
          health: fighter.health,
          power: fighter.power,
          defense: fighter.defense,
        },
      });
    } catch (err) {
      res.status(404);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    try {
      const { name, health, power, defense } = req.body;
      const fighterName = FighterService.search(name);

      if (fighterName) {
        return res.status(409).json({
          status: "error",
          code: 409,
          data: "Conflict",
          message: "Name is already use",
        });
      }
      const newFighter = FighterService.createNew({
        ...req.body,
      });
      return res.status(201).json({
        status: "success",
        code: 201,
        data: {
          id: newFighter.id,
          name: newFighter.name,
          health: newFighter.health,
          power: newFighter.power,
          defense: newFighter.defense,
        },
      });
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    try {
      const { id } = req.params;
      const dataToUpdate = req.body;
      const updatedFighter = FighterService.update(id, dataToUpdate);
      return res.json({
        status: "Success",
        code: 200,
        message: "Users information has just updated",
        data: {
          updatedFighter,
        },
      });
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const { id } = req.params;
      const deletedFighter = FighterService.delete(id);
      if (deletedFighter) {
        return res.json({
          status: "Success",
          code: 200,
          message: "Fighter deleted",
          data: {
            deletedFighter,
          },
        });
      } else {
        return res.status(404).json({
          error: true,
          status: "Error",
          code: 404,
          message: "Not Found",
        });
      }
    } catch (err) {
      res.status(500);
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
