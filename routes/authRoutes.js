const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      // TODO: Implement login action (get the user if it exist with entered credentials)
      const user = req.body;
      const data = AuthService.login(user);

      if (!data) {
        return res.status(401).json({
          status: "error",
          code: 401,
          data: "Unauthorized",
          message: "Invalid credentals",
        });
      }

      res.data = data;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
